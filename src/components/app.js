import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import './app.css';
import Holding from './holding.js';
import AccountTypeDetails from './account-type-details.js';
import { positions } from '../data/positions.js';
import { accounts } from '../data/accounts.js';

//get account holdings sum by account id
const getAccountHoldingsValueByAccountId = (holdings) => {
    return holdings.reduce((accounts, holding) => {
        if(!accounts[holding.account_id]) {
            accounts[holding.account_id] = 0;
        }

        accounts[holding.account_id] += holding.price * holding.quantity;

        return accounts;
    }, {});
}

//returns object with accountTypes and totalValue
//accountTypes -> object with account type and its total values
//totalValue -> sum of total values from all holdings
const getAccountTypeGroupedSumDetails = (positions, accounts) => {
    const accountHoldingsValueByAccountId = getAccountHoldingsValueByAccountId(positions);

    return accounts.reduce((details, acct) => {
        const accountHoldingsValue = accountHoldingsValueByAccountId[acct.id];
        //for each account type sum up holdings value
        if (!details.accountTypes[acct.type]) {
            details.accountTypes[acct.type] = accountHoldingsValue;
        } else {
            details.accountTypes[acct.type] += accountHoldingsValue;
        }

        //add holdings value to totals
        details.totalValue += accountHoldingsValue;

        return details;
    }, { accountTypes: {}, totalValue: 0 });
};

export default class App extends Component {
    constructor(props) {
        super(props);

        //get sum details grouped by account type 
        const accountTypeGroupedSumDetails = getAccountTypeGroupedSumDetails(positions, accounts);
        //overall sum across all holdings
        const totalValue = accountTypeGroupedSumDetails.totalValue;
        //map grouped details to array
        const accountTypeDetails = Object.keys(accountTypeGroupedSumDetails.accountTypes).map(accountType => {
            const sum = accountTypeGroupedSumDetails.accountTypes[accountType];

            return {
                type: accountType,
                perc_total: (sum / totalValue) * 100,
                sum
            };
        });

		this.state = {
            positions,
            accountTypeDetails
		};
	}
    render() {
        return (
            <div className="app">
                <div className="tile">
                    <div className="panel holdings">
                        <p className="panel-heading">
                            Holdings
                        </p>
                        <div className="panel-block">
                            <Holding positions={this.state.positions} />
                        </div>
                        <div className="panel-footer"/>
                    </div>
                </div>
                <div className="tile">
                    <div className="panel account-type-details">
                        <p className="panel-heading">
                            Account Type Details
                        </p>
                        <div className="panel-block">
                            <AccountTypeDetails data={this.state.accountTypeDetails} />
                        </div>
                        <div className="panel-footer"/>
                    </div>
                </div>
            </div>
        )
    }
};
