import React from 'react';
import PropTypes from 'prop-types';
import { formatNumber } from '../utility';

const Holdings = ({ positions }) => {
	return (
		<table className="table is-striped table is-hoverable">
			<thead>
				<tr>
					<th title="Account Id">Account Id</th>
					<th title="Ticker Name">Ticker Name</th>
					<th title="Ticker">Ticker</th>
					<th className="has-text-right" title="Price">Price</th>
					<th className="has-text-right" title="Quantity">Quantity</th>
				</tr>
			</thead>
			<tbody>
				{
					positions.map((holding) => (
						<tr key={holding.id}>
							<td>{holding.account_id}</td>
							<td className="has-text-weight-bold">{holding.ticker_name}</td>
							<td>{holding.ticker}</td>
							<td className="has-text-right">${formatNumber(holding.price)}</td>
							<td className="has-text-right">{holding.quantity}</td>
						</tr>
					))
				}
			</tbody>
		</table>
	)
};

Holdings.propTypes = {
	positions: PropTypes.array.isRequired
};

export default Holdings;
