import React from 'react';
import PropTypes from 'prop-types';
import { positions } from '../data/positions.js';
import { formatNumber } from '../utility';

//type | sum | percent of total
const AccountTypeDetails = ({ data }) => {
	return (
		<table className="table is-striped table is-hoverable">
			<thead>
				<tr>
					<th title="Type">Type</th>
					<th className="has-text-right" title="Sum">Sum</th>
					<th className="has-text-right" title="Percentage of Total">Percentage of Total</th>
				</tr>
			</thead>
			<tbody>
				{
					data.map((i) => (
						<tr key={i.type}>
							<td className="has-text-weight-bold">{i.type}</td>
							<td className="has-text-right">${formatNumber(i.sum)}</td>
							<td className="has-text-right">{formatNumber(i.perc_total)}%</td>
						</tr>
					))
				}
			</tbody>
		</table>
	)
}

AccountTypeDetails.propTypes = {
	data: PropTypes.array.isRequired
};

export default AccountTypeDetails;
